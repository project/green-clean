<?php // $Id$; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <meta http-equiv="content-language" content="<?php print $language->language; ?>" />
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if IE]><link rel="stylesheet" href="<?php print $path; ?>css/ie.css" type="text/css"><![endif]-->
  <!--[if lte IE 6]><link rel="stylesheet" href="<?php print $path; ?>css/ie6.css" type="text/css"><![endif]-->
</head>

<body class="<?php print $body_classes; ?>">
  <div id="page">
    <div id="page-inner">

      <?php if ($primary_links): ?>
      <div id="navigation">
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
        <?php if ($secondary_links) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    
    <div id="header">
      <?php if ($header || $search_box): ?>
        <div id="header-right">
          <?php print $header; ?>
          <?php print $search_box; ?>
        </div>
      <?php endif; ?>
      
      <div class="logo">
      <?php if ($logo): ?>
        <a id="logo" href="<?php print $base_path; ?>">
          <?php print theme('image', $logo); ?>
        </a><br class="clear" />
      <?php endif; ?>
      <?php if ($site_name): ?>
        <a id="site-name" href="<?php print $base_path; ?>">
          <?php print $site_name; ?>
        </a><br>      
        <?php if ($site_slogan): ?>
        <span id="slogan"><?php print $site_slogan; ?></span>
      <?php endif; ?><br class="clear" />
      <?php endif; ?>
      </div>

   	  <?php if ($before_content): ?>
          <div class="before"><?php print $before_content; ?></div>
      <?php endif; ?>
    </div>

    <?php print $breadcrumb; ?>

    <?php if ($left): ?>
      <div id="left" class="sidebar">
        <?php print $left; ?>
      </div>
    <?php endif; ?>

    <div id="centre">
      <span class="cap">&nbsp;</span>
      <div class="inner-centre">
      <?php print $messages; ?>

      <?php if ($tabs): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>

      <?php if ($title): ?>
        <h1 class="page-title"><?php print $title; ?></h1>
      <?php endif; ?>

      <?php print $help; ?>
      <?php print $content; ?>
      <?php print $feed_icons; ?>
       </div><!--// end inner-centre -->
    <span class="foot">&nbsp;</span>
    </div><!--// centre -->

    <?php if ($footer): ?>
      <div id="footer"><?php print $footer; ?></div>
    <?php endif; ?>

    <?php if ($footer_message): ?>
      <div id="footer-message"><?php print $footer_message; ?></div>
    <?php endif; ?>
     </div>
  </div><!--// page -->
  <?php print $closure; ?>
</body>
</html>
