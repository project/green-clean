
AUTHOR/MAINTAINERS 
------------------ 
Green Clean Theme by Marcus Stong, aka, Markku, aka, Stongo
http://eclipsemediaco.net

Adapted from base theme created by Richard Burford, aka, psynaptic http://freestylesystems.co.uk

DESCRIPTION 
-----------

Green Clean is a tableless layout with a left sidebar built on a "Clean" theme base. It's compatible
with IE6+, Firefox, Safari and Chrome. The Drupal 6.x version comes with two sub-themes, Green Clean
Light & Green Clean Lean. 
Green Clean Light uses a smaller background image for those concerned about load time. 
Green Clean Lean is stripped down and super fast loading. 
All themes contain the following block areas: left, before content, content, header, footer, 
footer message.